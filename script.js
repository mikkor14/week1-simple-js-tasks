//For ease of read, functions will be written in the old fashion way with function keyword

function fizzBuzz(number) {

    switch (true) {
        case (number % 15 === 0):
            console.log("FizzBuzz");
            break;
        case (number % 3 === 0):
            console.log("Fizz");
            break;
        case (number % 5 === 0):
            console.log("Buzz");
            break;
        default:
            console.log(number);
    }
}

function  fizzBuzzLoop(loopNumber) {
    for (let i=1; i < loopNumber; i++){
        fizzBuzz(i);
    }
}

function fibbFunction(number){

    let a = 1;
    let b = 0;
    let current;

    while (number >= 0){
        current = a;
        a = a + b;
        b = current;
        console.log(current);
        number--;
    }
}

fizzBuzzLoop(50);
console.log("------------------------");
console.log("NEW FUNCTION STARTS HERE");
console.log("------------------------");
fibbFunction(50);